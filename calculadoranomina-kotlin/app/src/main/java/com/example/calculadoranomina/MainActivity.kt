package com.example.calculadoranomina

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private var btnIngresar: Button? = null
    private var btnSalir: Button? = null
    private var txtUsuario: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar!!.setOnClickListener { ingresar() }
        btnSalir!!.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        btnSalir = findViewById<Button>(R.id.btnSalir)
        btnIngresar = findViewById<Button>(R.id.btnIngresar)
        txtUsuario = findViewById<EditText>(R.id.txtUsuario)
    }

    private fun ingresar() {
        val strUsuario = txtUsuario!!.text.toString()
        val bundle = Bundle()
        bundle.putString("usuario", strUsuario)
        val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun salir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora Nomina")
        confirmar.setMessage("¿Desea salir de la aplicación?")
        confirmar.setPositiveButton("Confirmar") { dialogInterface: DialogInterface?, which: Int -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialogInterface: DialogInterface?, which: Int -> }
        confirmar.show()
    }
}