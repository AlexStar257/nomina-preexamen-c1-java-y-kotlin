package com.example.calculadoranomina;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private EditText txtNumRecibo, txtNombre, txtHorasNormales, txtHorasExtra;
    private RadioGroup rdbPuesto;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private TextView lblSubtotalTotal, lblImpuestoTotal, lblTotalTotal, lblUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        iniciarComponentes();

        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    calcularNomina();
                } else {
                    mostrarToast("Por favor, completa todos los campos");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }


    private void iniciarComponentes(){
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasNormales = findViewById(R.id.txtHorasNormales);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);
        rdbPuesto = findViewById(R.id.rdbPuesto);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal);
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal);
        lblTotalTotal = findViewById(R.id.lblTotalTotal);
        lblUsuario = findViewById(R.id.lblUsuario);
    }

    private boolean validarCampos() {
        String numRecibo = txtNumRecibo.getText().toString();
        String nombre = txtNombre.getText().toString();
        String horasNormales = txtHorasNormales.getText().toString();
        String horasExtra = txtHorasExtra.getText().toString();
        RadioButton selectedRadioButton = findViewById(rdbPuesto.getCheckedRadioButtonId());

        return !numRecibo.isEmpty() && !nombre.isEmpty() && !horasNormales.isEmpty() && !horasExtra.isEmpty() && selectedRadioButton != null;
    }

    private void calcularNomina() {
        // Obtener los valores de entrada de los EditTexts
        int numRecibo = Integer.parseInt(txtNumRecibo.getText().toString());
        String nombre = txtNombre.getText().toString();
        float horasNormales = Float.parseFloat(txtHorasNormales.getText().toString());
        float horasExtra = Float.parseFloat(txtHorasExtra.getText().toString());

        // Obtener el texto del radio button seleccionado
        RadioButton selectedRadioButton = findViewById(rdbPuesto.getCheckedRadioButtonId());
        int puesto = 0;

        if (selectedRadioButton != null) {
            String puestoText = selectedRadioButton.getText().toString();

            if (puestoText.equalsIgnoreCase("Auxiliar")) {
                puesto = 1;
            } else if (puestoText.equalsIgnoreCase("Albañil")) {
                puesto = 2;
            } else if (puestoText.equalsIgnoreCase("Ing Obra")) {
                puesto = 3;
            }
        }

        ReciboNomina reciboNomina = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f);

        // Realizar los cálculos necesarios en el objeto reciboNomina
        float subtotal = reciboNomina.calcularSubtotal();
        float impuesto = reciboNomina.calcularImpuesto();
        float total = reciboNomina.calcularTotal();

        // Mostrar los valores calculados en los TextViews
        lblSubtotalTotal.setText(String.valueOf(subtotal));
        lblImpuestoTotal.setText(String.valueOf(impuesto));
        lblTotalTotal.setText(String.valueOf(total));
    }

    private void limpiarCampos() {
        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasNormales.setText("");
        txtHorasExtra.setText("");
        rdbPuesto.clearCheck();
        lblSubtotalTotal.setText("");
        lblImpuestoTotal.setText("");
        lblTotalTotal.setText("");
    }
    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora Nomina");
        confirmar.setMessage("Regresar al Menú Principal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

}
